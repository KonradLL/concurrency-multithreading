package data_structures.implementation;

public interface LinkedListInterface<T> {
    /**
     * Specification for the Linked List Data Structure
     *
     * Elements: Objects of type T
     * Structure: Linear
     * Domain: N/A
     *
     * PRECONDITION: N/A
     * POSTCONDITION: A new ListNode Object has been created
     * ListNode();
     *
     * PRECONDITION: N/A
     * POSTCONDITION: A new ListNode object has been created and contains a copy of the data from the src ListNode object
     * ListNode(ListNode<T> src)
     *
     */

    /**
     * PRECONDITION: N/A
     * POSTCONDITION: A new element of type T has been added to the ListNode Object
     */

    void setElement(T newElement);

    /**
     * PRECONDITION: N/A
     * POSTCONDITION: A new LinkedList pointer to the next ListNode Object has been added.
     */

    void setNextPtr(ListNode<T> next);

    /**
     * PRECONDITION: N/A
     * POSTCONDITION: The element of type T contained in the ListNode object has been returned.
     */

    T getElement();

    /**
     * PRECONDITION: N/A
     * POSTCONDITION: The pointer to the next ListNode Object has been returned.
     */

    ListNode<T> getNext();








}