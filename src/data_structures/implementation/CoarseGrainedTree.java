package data_structures.implementation;

import java.util.ArrayList;
import java.util.concurrent.locks.*;

import data_structures.Sorted;

public class CoarseGrainedTree<T extends Comparable<T>> implements Sorted<T> {
    
    private BinaryTreeNode root;

    private final Lock lock = new ReentrantLock();

    // Acquire lock to access data structure, attempt to add an element, unlock structure when finished.
    public void add(T t) {
        lock.lock();

        try{
            root = addNode(root, t);
        }
        catch(Exception e){
            e.printStackTrace();
        }
        finally {
            lock.unlock();
        }
    }

    // Acquire lock to access data structure, attempt to remove element, unlock structure when finished.
    public void remove(T t) {
        lock.lock();

        try{
            root = removeNode(root, t);
        }
        catch(Exception e){
            e.printStackTrace();
        }
        finally {
            lock.unlock();
        }
    }

    // Calls toList with an empty ArrayList to get all elements from the tree.
    public ArrayList<T> toArrayList() {
        ArrayList<T> arrayList = new ArrayList<>();

        return toList(root, arrayList);
    }
    
    // TODO:: change this structure to a while loop instead of recursive since number of nodes may cause performance issues
    private BinaryTreeNode addNode(BinaryTreeNode current, T t){
        if(current == null) {
            current = new BinaryTreeNode();
            current.value = t;
            return current;
        }
        else if(current.value.compareTo(t) <= 0) {
            current.left = addNode(current.left, t);
        }
        else {
            current.right = addNode(current.right, t);
        }

        return current;
    }
    // TODO:: change to while from recursion due to performance issues once node count gets too great
    private BinaryTreeNode removeNode(BinaryTreeNode current, T t){
        if(current == null){
            return null;
        }
        
        if(current.value.compareTo(t) == 0) {
            if(current.left == null && current.right == null){
                return null;
            }
            else if(current.left == null) {
                return current.right;
            }
            else if(current.right == null) {
                return current.left;
            }
            else{
                T smallestValue = findSmallestValue(current.right);
                current.value = smallestValue;
                current.right = removeNode(current.right, smallestValue);
                return current;
            }
        }
        else if(current.value.compareTo(t) < 0){
            current.left = removeNode(current.left, t);
            return current;
        }
        current.right = removeNode(current.right, t);
        return current;
    }
    
    // rewrite code to not be recursive and not use ternary statment because it's not very readable.
    private T findSmallestValue(BinaryTreeNode root){
        return (root.left == null) ? (T) root.value : findSmallestValue(root.left);
    }

    private ArrayList<T> toList(BinaryTreeNode node, ArrayList<T> arrayList){
        if(node != null){
            toList(node.right, arrayList);
            arrayList.add((T)node.value);
            toList(node.left, arrayList);
        }

        return arrayList;
    }
}