package data_structures.implementation;

import java.util.ArrayList;
import java.util.concurrent.locks.*;
import data_structures.Sorted;

public class FineGrainedTree<T extends Comparable<T>> implements Sorted<T> {

    // starting to think it is necessary for us to implement locking functions inside the node class to make this work as we want to.
    // this would allow a thread to lock a specific node for while it is not necessarily working with that node at the time, for example when removing a node.
    // may be simpler to handle the locking mechanisms inside the tree using loops rather than locks.

    // looking at some online resources it seems that some people implement the locks for the whole tree, so all threads will acquire the read lock for the whole structure, then when they have found where
    // they need to write they will attempt to get acquire a write lock that locks the whole structure for writing, this may end up in a little overhead due to the whole structure being locked on every write
    // however reduces the overhead of reacquiring the read lock on every node

    // reentrant lock gave the best solution for now, read write lock if properly implemented however should result in a faster system due to less locking overhead.
    // optimistic locking or lazy locking would likely be the fastest method to lock for a tree, however implementing that would take quite some time
    // and that it sime we likely do not have.

    private final Node<T> guardNode = new Node<>(); // Guard Node that left links to root and has no value
    private Node<T> root;

    public FineGrainedTree(){ }

    // for add the empty list is a special case, if no element is in the list a lock must be acquired to add that element.
    public void add(T t) {
        guardNode.lock.lock();

        try{
            if(guardNode.left == null){ // left path from the head (guard) node will lead into root
                root = new Node<>();
                guardNode.left = root;
                root.value = t;
            }
            else{
                root.lock.lock();
                addNode(guardNode, root, t);
            }
        } finally {
            try{
                guardNode.lock.unlock();
            }catch (Exception ignored){ } // do nothing since we don't care if we fail at unlocking at this point
        }
    }

    public void remove(T t) {
        guardNode.lock.lock();
        try{
            if(guardNode.left == null){ guardNode.lock.unlock(); }
            else{
                root.lock.lock();
                root = removeNode(root, root, t);
            }
        } finally {
            try{
                guardNode.lock.unlock();
            }catch (Exception ignored){ } // do nothing because failing to unlock at this point does not matter
        }
    }

    public ArrayList<T> toArrayList() {
        ArrayList<T> arrayList = new ArrayList<>();
        return toList(root, arrayList);
    }

    // recursion is inherently risky because it returns nodes throughout the tree, however as long as nodes do not change the contents of the objects
    // this should be considered to be ok, have to find a way to solve the problem of the empty tree existing so no two threads try to assign root at the same time.
    // made recursive function void as it does not need to return a value since the new node assigment is done within he function instead of returned.
    // current node always performs an safety unlock of itself even if the next function also unlocks it, this also performs the final unlock in the add funciton.
    // also makes sure the handover locking works properly.
    private void addNode(Node<T> previous, Node<T> current, T t){
        previous.lock.unlock();

        try{
            if(current.value.compareTo(t) <= 0){
                if(current.left == null){
                    current.left = new Node<>();
                    current.left.value = t;
                }
                else{
                    current.left.lock.lock();
                    addNode(current, current.left, t);
                }
            }
            else{
                if(current.right == null){
                    current.right = new Node<>();
                    current.right.value = t;
                }
                else{
                    current.right.lock.lock();
                    addNode(current, current.right, t);
                }
            }
        } finally {
            try{
                current.lock.unlock();    // this unlock will unlock when the final node has been added all other cases it will throw an error but it'll be ignored
            }catch (Exception ignored){ } // we don't care if we fail to unlock current in this case
        }
    }

    // recursive solution for node removal working properly.
    private Node<T> removeNode(Node<T> previous, Node<T> current, T t){
        current.lock.lock();
        previous.lock.unlock();

        try {
            if (current.value.compareTo(t) == 0) {
                if (current.left == null) { return current.right; }
                else if (current.right == null) { return current.left; }
                else {
                    current.lock.lock();        // activating a second lock on current to prevent it from being unlocked when findsmallest is called.
                    current.right.lock.lock();

                    T smallestValue = findSmallest(current, current.right);

                    current.value = smallestValue;
                    current.right = removeNode(current, current.right, smallestValue);

                    return current;
                }
            }
            else if (current.value.compareTo(t) < 0) { current.left = removeNode(current, current.left, t); }
            else { current.right = removeNode(current, current.right, t); }

            return current;
        } finally {
            try{
                current.lock.unlock();
            }catch (Exception ignored) { } // ignore failed unlock
        }
    }


    // parent node should be locked preventing any node from passing through, however this may overtake some slow thread looking for the element this finds which may cause problems.
    // this means bottom node must also be made inaccessible to other nodes while the recursive calls are happening
    // previous statement is false, because we always lock 2 nodes at a time and do handover locking this function could never overtake another thread.
    private T findSmallest(Node<T> previous, Node<T> current){
        previous.lock.unlock();

        try{
            if(current.left == null){ return current.value; }
            current.left.lock.lock();
            return findSmallest(current, current.left);
        } finally {
            try{
                current.lock.unlock();
            }catch (Exception ignored){ } // ignoring a failed unlock
        }

    }

    private ArrayList<T> toList(Node<T> node, ArrayList<T> arrayList){
        if(node != null){
            toList(node.right, arrayList);
            arrayList.add(node.value);
            toList(node.left, arrayList);
        }

        return arrayList;
    }

    // reentrant lock set to true means that it tries to be fair, allowing the longest waiting thread to acquire the lock before other threads
    // this is does not guarantee starvation free-ness but goes a far way to reduce the likelihood of a thread getting starved.
    private class Node<E extends Comparable<T>> {
        private final Lock lock = new ReentrantLock(true);

        E value;
        Node<E> left;
        Node<E> right;

        Node() {
            this.value = null;
            right = null;
            left = null;
        }
    }
}
