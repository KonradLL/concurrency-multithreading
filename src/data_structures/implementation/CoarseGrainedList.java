package data_structures.implementation;

import java.util.ArrayList;

import data_structures.Sorted;


import java.util.concurrent.locks.*;

public class CoarseGrainedList<T extends Comparable<T>> implements Sorted<T> {

    //    private static int sizeOfList = 0;
    private ListNode<T> root = new ListNode<>();
    public Lock lock = new ReentrantLock();

    public void add(T t) {
        lock.lock();

        try {
            addElement(t);
        } finally {
            lock.unlock();
        }
    }

    private void addElement(T t) {

        if (this.root.isEmpty()) {
            ListNode<T> nw = new ListNode(t);
            nw.setNext(this.root);
            this.root = nw;
            return;
        // else if t smaller than root, replace root
        } else if (t.compareTo(root.getElement()) < 1) {
            ListNode<T> nw = new ListNode(t);
            nw.setNext(this.root);
            this.root = nw;
            return;
        } else if (t.compareTo(root.getElement()) == 0) {
            root.incrementAmount();
            return;
        }


        ListNode<T> curr = this.root;
//        ListNode<T> prev = new ListNode();
        while (!curr.isEmpty()) {
            if (curr.getNext().isEmpty()) {
                ListNode<T> nw = new ListNode(t);
                nw.setNext(curr.getNext());
                curr.setNext(nw);
                return;
            }
            // -1 t smaller than arg, +1 t bigger than arg, 0 t equal to arg
            int compareResult = t.compareTo(curr.getNext().getElement());
            // If current T is smaller than toAdd T, add to left

            // Else If elements is equal to value found, increment node #
            if (compareResult == 0) {
                curr.getNext().incrementAmount();
                return;
            // Else go to next
            } else if (compareResult < 0) {
                ListNode<T> nw = new ListNode(t);
                nw.setNext(curr.getNext());
                curr.setNext(nw);
                return;
            } else {
                curr = curr.getNext();
            }
        }
        throw new UnsupportedOperationException();
    }

    public void remove(T t) {
        lock.lock();
        try {
            removeElement(t);
        } finally {
            lock.unlock();
        }
    }

    private void removeElement(T t) {
        ListNode<T> curr = this.root;
        ListNode<T> prev = new ListNode();

        while (!curr.isEmpty()) {
            // If found T is equal to toRemove T
            if (t.compareTo(curr.getElement()) == 0) {
                // If # is bigger than 1, decrement by 1
                if (!curr.decrementAmount()) {
                    if(curr == this.root) {
                        this.root = curr.getNext();
                    } else {
                        prev.setNext(curr.getNext());
                    }
                }
                return;
            } else {
                prev = curr;
                curr = curr.getNext();
            }
        }
    }

    public ArrayList<T> toArrayList() {
        ArrayList<T> list = new ArrayList();

        ListNode<T> curr = this.root;
        int k = 0;
        while (!curr.isEmpty()) {
            k++;
            //Add the item # of times it appears in the node
            for(int i = 1; i <= curr.getAmount(); i++) {
                list.add(curr.getElement());
            }
            curr = curr.getNext();
        }
//        System.out.println("amount of nodes: " + k);
        return list;
    }
}