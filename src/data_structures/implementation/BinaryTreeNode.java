package data_structures.implementation;

public class BinaryTreeNode<T extends Comparable<T>>{

	T value;
	BinaryTreeNode left;
	BinaryTreeNode right;

	public void BinaryTreeNode(){
		this.value = null;
		right = null;
		left = null;
	}
}
