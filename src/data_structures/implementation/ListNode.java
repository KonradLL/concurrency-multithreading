package data_structures.implementation;

public class ListNode<T> {

    private T element;
    private int amount;
    private ListNode<T> next;

    public ListNode() {
        this.amount = 0;
    }

    public ListNode(T element) {
        this.element = element;
        this.amount = 1;
    }

    public Boolean isEmpty() {
        return (this.amount <= 0) ? true : false;
    }

    public void incrementAmount() {
        this.amount++;
    }

    public boolean decrementAmount() {
        if(this.amount <= 1) {
            return false;
        } else {
            this.amount--;
            return true;
        }
    }

    public void setNext(ListNode next) {
        this.next = (ListNode<T>) next;
    }

    public T getElement() {
        return this.element;
    }

    public int getAmount() {
        return amount;
    }

    public ListNode<T> getNext() {
        return this.next;
    }

    @Override
    public String toString() {
        return "value: " + this.element + " amount: " + this.amount;
    }
}