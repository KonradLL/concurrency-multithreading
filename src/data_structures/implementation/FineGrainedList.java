package data_structures.implementation;

import java.util.ArrayList;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;
import java.io.StringWriter;
import java.io.PrintWriter;

import data_structures.Sorted;

public class FineGrainedList<T extends Comparable<T>> implements Sorted<T> {

    private Node<T> root = new Node<>();
    private ReentrantLock entrantLock = new ReentrantLock();

    public void add(T t) {
//        System.out.println("add: " + t);
        try {
            addElement(t);
        } catch(Exception err) {
            System.out.println("add error on:" + t);

            StringWriter sw = new StringWriter();
            PrintWriter pw = new PrintWriter(sw);
            err.printStackTrace(pw);
            String sStackTrace = sw.toString(); // stack trace as a string
            System.out.println(sStackTrace);
        }
    }

    private void addElement(T t) {
        Node<T> newNode = new Node(t);

        entrantLock.lock();
        this.root.lock();

        if (this.root.isEmpty()) {
            this.root.setElement(t);
            this.root.unlock();
            entrantLock.unlock();
            return;
        // else if t smaller than root, replace root
        } else if (t.compareTo(root.getElement()) < 1) {
            newNode.setNext(this.root);
            newNode.lock();
            this.root = newNode;
            newNode.getNext().unlock();
            newNode.unlock();
            entrantLock.unlock();
            return;
        }

        Node<T> curr = this.root;
        entrantLock.unlock();
        while (curr != null) {
            if (curr.getNext() == null) {
                curr.setNext(newNode);
                curr.unlock();
                return;
            } else {
                curr.getNext().lock();
            }

            // -1 t smaller than arg, +1 t bigger than arg, 0 t equal to arg
            int compareResult = t.compareTo(curr.getNext().getElement());

            if (compareResult < 1) {
                newNode.setNext(curr.getNext());
                curr.setNext(newNode);
                newNode.getNext().unlock();
                curr.unlock(); //problem
                return;
            } else {
                Node<T> next = curr.getNext();
                curr.unlock(); //problem
                curr = next;
            }
        }
        throw new UnsupportedOperationException();
    }

    public void remove(T t) {
//        System.out.println("remove: " + t);
        try {
            removeElement(t);
        } catch(Exception err) {
            System.out.println("remove err on:" + t);

            StringWriter sw = new StringWriter();
            PrintWriter pw = new PrintWriter(sw);
            err.printStackTrace(pw);
            String sStackTrace = sw.toString(); // stack trace as a string
            System.out.println(sStackTrace);
        }
    }

    private void removeElement(T t) {

        entrantLock.lock();
        this.root.lock();

        if (t.compareTo(root.getElement()) == 0) {
            if(root.getNext() == null){
                root.setElement(null);
            } else {
                root.getNext().lock();
                Node<T> temp = root;
                this.root = this.root.getNext();
                temp.unlock();
            }
            root.unlock();
            entrantLock.unlock();
            return;
        }

        Node<T> curr = this.root;
        entrantLock.unlock();
        while (curr.getNext() != null) {
            curr.getNext().lock();
            // If found T is equal to toRemove T
            if (t.compareTo(curr.getNext().getElement()) == 0) {
                Node<T> oldNext = curr.getNext();
                Node<T> nextNext = curr.getNext().getNext();
                if(nextNext == null) {
                    curr.setNext(null);
                } else {
                    curr.setNext(curr.getNext().getNext());
                }
                oldNext.unlock();
                curr.unlock();
                return;
            } else {
                Node<T> next = curr.getNext();
                curr.unlock(); //problem
                curr = next;
            }
        }
        curr.unlock();
    }

    public ArrayList<T> toArrayList() {
        ArrayList<T> list = new ArrayList();

        Node<T> curr = this.root;
        while (curr != null) {
            if(!curr.isEmpty())
                list.add(curr.getElement());

            if (curr.getNext() != null) {
                curr = curr.getNext();
            } else { break; }
        }
        return list;
    }

    private class Node<T> {

        private T element;
        private Node<T> next;
        private Lock lock = new ReentrantLock();

        public Node() {
            this.element = null;
        }

        public Node(T element) {
            this.element = element;
        }

        public Boolean isEmpty() {
            return (this.element == null) ? true : false;
        }

        public void setElement(T element) {
            this.element = element;
        }

        public T getElement() {
            return this.element;
        }

        public void setNext(Node next) {
            this.next = (Node<T>) next;
        }

        public Node<T> getNext() {
            return this.next;
        }

        public void lock() {
            this.lock.lock();
        }

        public void unlock() {
            this.lock.unlock();
        }

        @Override
        public String toString() {
            return "value: " + this.element;
        }
    }
}
